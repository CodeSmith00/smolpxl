# Changelog

## [0.4.0] 2021-01-28

- Allow changing screen size in the view method

## [0.3.1] 2021-01-26

- Remove extra export statement

## [0.3.0] 2021-01-26

- Provide getters for properties of screen, to make life easier in WASM

## [0.2.2] 2021-01-09

- Copy values that are passed in to Game's methods, so we don't rely on them
  being available later.  (Steps towards being usable from WASM.)

## [0.2.1] 2021-01-08

- If page has already loaded, call onLoad immediately

## [0.2] 2021-01-08

- Include button images within JS, simplifying deployment

## [0.1] 2021-01-01

- Prototype of Rightwaves game
- First version of games: Spring, Snake, Life, Heli, Duckmaze2
- Ability to report play statistics
- Buttons to Like, Share etc.
- Text labels
- Basic menu system
- Main game loop: model, update, view
- ASCII-art for images
- Game mechanics: frame rate handling, canvas for drawing
