"use strict";

const WIDTH = 100;
const HEIGHT = 96;
const BOTTOM_BAR_HEIGHT = 4;
const PHASER_OUTLINE_COL = [126, 125, 125];
const PHASER_TOP_COL = [18, 77, 144];
const PHASER_BOTTOM_COL = [11, 48, 90];
const PHASER_END_COL = [49, 126, 215];
const SCROLL_SPEED = 0.5;

const MOVE_L = -1;
const MOVE_R = 1;
const MOVE_D = 1;
const MOVE_U = -1;
const STOP_L = -0.1;
const STOP_R = 0.1;

const IMAGES = {
    "ship": {
        pixels: [
            "..www.......",
            "dddddww.bbb.",
            "rllldddcccwb",
            "dddhlllccccb",
            "rddddddaccb.",
            "..aaa......."
        ],
        key: {
            "w": [255, 255, 255],
            "d": [88, 88, 88],
            "b": [77, 111, 249],
            "r": [141, 0, 0],
            "l": [126, 125, 125],
            "h": [192, 192, 192],
            "c": [3, 157, 157],
            "a": [42, 42, 42]
        }
    },
    "ship-nose-attach": {
        "pixels": [
            "a...a",
            "bcdeb",
            "beedb",
            "bceeb",
            "bcccb",
            "a...a"
        ],
        "key": {
            "a": [126, 125, 125],
            "b": [88, 88, 88],
            "c": [51, 90, 249],
            "d": [142, 163, 249],
            "e": [77, 111, 249]
        }
    },
    "ship-death-01": {
        "pixels": [
            "................",
            "....aaa.........",
            "...abbba....a...",
            "..abcbbba.aba...",
            ".aabbddcbbbcba..",
            "..bbcbbbecdcbf..",
            "..aaaabbaabba...",
            "...agaaaa.aa....",
            "................",
            "................"
        ],
        "key": {
            "a": [255, 79, 79],
            "b": [255, 194, 79],
            "c": [246, 255, 27],
            "d": [255, 255, 255],
            "e": [126, 125, 125],
            "f": [77, 111, 249],
            "g": [42, 42, 42]
        }
    },
    "ship-death-02": {
        "pixels": [
            ".....a...aa.....",
            "..ab.b.cbcbaa...",
            "...bddbeebebeb..",
            ".abeeeefeefdeea.",
            ".abdefffdcffedb.",
            ".aeeeeeedeefdba.",
            "..bdeedebbbeeea.",
            "..cbcbbcbceeba..",
            "..a.a.a...aaa...",
            "................"
        ],
        "key": {
            "a": [150, 46, 46],
            "b": [182, 139, 57],
            "c": [255, 79, 79],
            "d": [246, 255, 27],
            "e": [135, 135, 135],
            "f": [73, 73, 73]
        }
    },
    "ship-death-03": {
        "pixels": [
            "....ab.a.b.aa...",
            "..baababbaabbaa.",
            "..aacaaaaaaacab.",
            ".baaccccaccccab.",
            ".baac.cccc..caab",
            "..aacccacccccaab",
            "..aaccaaaacaaab.",
            "..baaaaaaaaaaba.",
            "....babbaabab...",
            "................"
        ],
        "key": {
            "a": [135, 135, 135],
            "b": [150, 46, 46],
            "c": [73, 73, 73]
        }
    },
    "ship-death-04": {
        "pixels": [
            "................",
            "....abba..aaaa..",
            "..aab.baababbaa.",
            "..bb...bb.bbbba.",
            ".ab.....b...bb..",
            ".abb....b...bba.",
            "...bb.bbb...bba.",
            "..aaabbaabbbba..",
            "....a..a...ba...",
            "................"
        ],
        "key": {
            "a": [135, 135, 135],
            "b": [73, 73, 73]
        }
    },
    "ship-death-05": {
        "pixels": [
            "................",
            "................",
            "...a..a.........",
            "...aaa...a.aaa..",
            "...a.........a..",
            "...a..a..a...a..",
            "...aa..a....aa..",
            ".....a....aaaa..",
            "................",
            "................"
        ],
        "key": {
            "a": [73, 73, 73]
        }
    },
    "life": {
        "pixels": [
            ".aa.....",
            "bbbccdae",
            "fcgbbdde",
            ".hh....."
        ],
        "key": {
            "a": [255, 255, 255],
            "b": [126, 125, 125],
            "c": [88, 88, 88],
            "d": [3, 157, 157],
            "e": [77, 111, 249],
            "f": [141, 0, 0],
            "g": [192, 192, 192],
            "h": [42, 42, 42]
        }
    },
    "machinery-20x08-01": {
        pixels: [
            "..abbbbbbbbbbbbbbb..",
            ".aaaaaaaaaaaaaaaaab.",
            "aacacacacccccccccccb",
            "daaaaaaaaaaaaaaaaaab",
            "dddadddaaaaaddddaaaa",
            ".dddeeddaaadeeedddd.",
            ".eefefeddddeeeeeee..",
            ".eeeeeeeeeeeaeaeae.."
        ],
        key: {
            "a": [184, 184, 184],
            "b": [255, 255, 255],
            "c": [67, 63, 16],
            "d": [119, 118, 118],
            "e": [45, 57, 22],
            "f": [117, 0, 0]
        }
    },
    "machinery-20x08-02": {
        "pixels": [
            "..abbbbbbbbbbbbbbb..",
            ".accccacccccccacacb.",
            "aaaaaaaaaaaaaaaaaaab",
            "daaaaaaaaaaaaaaaaaab",
            "ddaaaaaaddaddadaaaaa",
            ".dddadaaaaaaaaaaaaa.",
            ".eeeeeaefefefeaeee..",
            ".eeeeeeeeeeeeeeeee.."
        ],
        "key": {
            "a": [184, 184, 184],
            "b": [255, 255, 255],
            "c": [67, 63, 16],
            "d": [119, 118, 118],
            "e": [45, 57, 22],
            "f": [117, 0, 0]
        }
    },
    "machinery-20x08-03": {
        "pixels": [
            "..abbbbbbbbbbbbbbb..",
            ".aaaaaaaaaaaaaaaaab.",
            "aaaaaaaaaaaaaaaaaaab",
            "cdddddddddabbbaddddb",
            "caaaeecaaacaaabaaaaa",
            ".caeeeecaaacccaaaaa.",
            ".eafefeeccaaaaaaaa..",
            ".eaeeeeeeeeeeeeeee.."
        ],
        "key": {
            "a": [184, 184, 184],
            "b": [255, 255, 255],
            "c": [119, 118, 118],
            "d": [67, 63, 16],
            "e": [45, 57, 22],
            "f": [117, 0, 0]
        }
    },
    "machinery-20x16-01": {
        "pixels": [
            "..abbbbbbbbbbbbbbb..",
            ".aaaaaaaaaaaaaaaaab.",
            "acccccdaaaaaaaaaaaaa",
            "..bbbbcdaaaaaaaaaaaa",
            ".daaaabccccccccccccd",
            ".daaaabedddddddadad.",
            ".daaaabfefeeeeedeae.",
            ".eddddeefeeaaaaaeae.",
            ".eeabeefefedddddeae.",
            ".eeeabeefeeaaaaaeaa.",
            ".eeeabefefedddddeee.",
            ".eeeabeefeccccceeee.",
            ".eeabeefefdaaabeege.",
            ".eabeeeefeddaaaeeee.",
            ".eabeeefefdaaaaeege.",
            ".eeabeeefeddaaaeeee."
        ],
        "key": {
            "a": [184, 184, 184],
            "b": [255, 255, 255],
            "c": [67, 63, 16],
            "d": [119, 118, 118],
            "e": [45, 57, 22],
            "f": [66, 66, 66],
            "g": [117, 0, 0]
        }
    },
    "machinery-20x24-01": {
        "pixels": [
            "..abbbbbbbbbbbbbbb..",
            ".aaaaaaaaaaaaaaaaab.",
            "acccccccccccccccccca",
            "daaaaaaaaaaaaaaaaaaa",
            "ddaaaaaaaaaaaaaaaaad",
            ".dddddddddddddddddd.",
            ".eeeeeeeaaaaaaaabb..",
            ".eeaffaeddddcacacb..",
            ".eeaffaeeaeadaaaab..",
            ".ebbbbeeeaeaedaaa...",
            ".aaaabbceaeaeedaabb.",
            ".addaececaeaee.daabb",
            ".eaaeeeceaeaee..daab",
            ".eeeeececaeeae...daa",
            ".eaaeeeaaeaeae....ac",
            ".adgaeadgaaeea....aa",
            ".addaeaddaeaeea...dc",
            ".eaaeeeaaeeeae.a..aa",
            ".eeeeeeeeeeeeagaggda",
            ".eddeeeddeeceaddaddd",
            ".eddeeeddececebgagg.",
            ".gddeegddeeceeddddd.",
            ".gddeegddececebgggg.",
            ".gddeegddeeceeddddd."
        ],
        "key": {
            "a": [184, 184, 184],
            "b": [255, 255, 255],
            "c": [67, 63, 16],
            "d": [119, 118, 118],
            "e": [45, 57, 22],
            "f": [117, 0, 0],
            "g": [66, 66, 66]
        }
    },
    "alien-red-flat": {
        "pixels": [
            ".......abb...",
            "......aaacb..",
            "....deeaacab.",
            "....aaaaaca..",
            ".ffbbbbbbbbb.",
            "fegfaaaaaaaaa",
            "feffaaaaaaaaa",
            ".ffccccccccc.",
            "....aaaaaca..",
            "....deeaacaa.",
            "......aaaca..",
            ".......caa..."
        ],
        "key": {
            "a": [196, 43, 38],
            "b": [225, 100, 97],
            "c": [121, 39, 37],
            "d": [255, 255, 255],
            "e": [69, 126, 53],
            "f": [110, 178, 91],
            "g": [170, 254, 146]
        }
    },
    "alien-yellow-line-315": {
        "pixels": [
            "......ab........",
            "......abbc......",
            ".....aaabbb.....",
            ".....daaefb.....",
            "....bdddfffaa...",
            "..affgggffaabb..",
            "aaaafhigffajfbb.",
            "ddaafklgdfdfffb.",
            ".ddfffffbddddfbb",
            ".cdffffffbbddddd",
            "..dddaabbbb.dd..",
            "....aafffb......",
            "....adffffb.....",
            ".....dddffb.....",
            "......dddd......",
            "........dd......"
        ],
        "key": {
            "a": [149, 149, 149],
            "b": [222, 216, 142],
            "c": [199, 0, 26],
            "d": [100, 97, 64],
            "e": [158, 154, 98],
            "f": [158, 154, 101],
            "g": [4, 88, 35],
            "h": [11, 157, 63],
            "i": [130, 252, 176],
            "j": [158, 154, 104],
            "k": [0, 157, 58],
            "l": [0, 157, 62]
        }
    },
    "alien-yellow-line-292": {
        "pixels": [
            "................",
            "...ab.c.........",
            "...aabbb........",
            "....aabbbba.....",
            "....daeeeeaebb..",
            "....ddfeeaeeebb.",
            "...dgffheeeeeebb",
            "...eegffdddeeeeb",
            ".aabbggfedddd...",
            "aaaeebeebeed....",
            ".dddebeaebb.....",
            "..cdeebaeebb....",
            "....deaeeeeb....",
            ".....daddeebb...",
            "........dddeb...",
            "................"
        ],
        "key": {
            "a": [149, 149, 149],
            "b": [222, 216, 142],
            "c": [196, 43, 38],
            "d": [100, 97, 64],
            "e": [158, 154, 101],
            "f": [4, 88, 35],
            "g": [6, 157, 63],
            "h": [130, 252, 176]
        }
    },
    "alien-yellow-line-270": {
        "pixels": [
            "................",
            "...........a....",
            "....ba..c.aaa...",
            ".caadaaacdddaaa.",
            ".cccdddacddddaa.",
            "..ccceddcedddd..",
            "...deffddeedd...",
            "...dgfhfeeee....",
            "...dggffdaaa....",
            "...ddggddddaa...",
            "..cccaaecdddaa..",
            ".cccddaacddddda.",
            ".ceeeddeceeddda.",
            "....be..c.eee...",
            "...........e....",
            "................"
        ],
        "key": {
            "a": [222, 216, 142],
            "b": [196, 43, 38],
            "c": [149, 149, 149],
            "d": [158, 154, 101],
            "e": [100, 97, 64],
            "f": [4, 88, 35],
            "g": [6, 157, 63],
            "h": [130, 252, 176]
        }
    },
    "alien-yellow-line-247": {
        "pixels": [
            "................",
            "........aaaaa...",
            ".....bcddaaaa...",
            "....bbcdddda....",
            "..edabbcdddd....",
            ".ddddabcddd.....",
            "cccddabbdbbd....",
            ".ccbbfffbddaa...",
            "...bbghfbdddaaaa",
            "...bgggfbbbddaaa",
            "....ddgdacbbdda.",
            "....bcdddacbbb..",
            "....ccbddac.....",
            "...ccbbb........",
            "...cd.e.........",
            "................"
        ],
        "key": {
            "a": [222, 216, 142],
            "b": [100, 97, 64],
            "c": [149, 149, 149],
            "d": [158, 154, 101],
            "e": [196, 43, 38],
            "f": [4, 88, 35],
            "g": [6, 157, 63],
            "h": [130, 252, 176]
        }
    },
    "alien-yellow-line-225": {
        "pixels": [
            "........aa......",
            "......aaaa......",
            ".....bccaaa.....",
            "....dbbccca.....",
            "....ddbbbc......",
            "..aaaddbbcc.aa..",
            ".eccaacccccccaaa",
            ".ccccacbccbcccaa",
            "bbddcfffbcbbcca.",
            "ddddbghfccdbbca.",
            "..dbbiifcaddbb..",
            "....bbbcccadd...",
            ".....bddjcc.....",
            ".....dddccc.....",
            "......dbce......",
            "......db........"
        ],
        "key": {
            "a": [222, 216, 142],
            "b": [100, 97, 64],
            "c": [158, 154, 101],
            "d": [149, 149, 149],
            "e": [199, 0, 26],
            "f": [4, 88, 35],
            "g": [11, 157, 63],
            "h": [130, 252, 176],
            "i": [0, 157, 58],
            "j": [158, 154, 98]
        }
    },
    "alien-tracks-1-right": {
        "pixels": [
            ".....aaabbcb......",
            "....aaaaabbcb.....",
            "...ddaaaaaaabb....",
            "..dddddaaadddd....",
            ".eddddddddddfddd..",
            ".eedddddddddfdddd.",
            "..eeddddddddfddddd",
            "...eddddddddffbbdd",
            "....edddddddffdbbb",
            "....eedddddffddaaa",
            ".....eeedddffdddad",
            ".......eedfdddddd.",
            ".......eghddeee...",
            "......dggghee.....",
            "......ddggee......",
            ".....dddgee.......",
            ".....ggggff.......",
            "...ddddffffff.....",
            "..dddddddddfff....",
            ".ddiijiijiijiif...",
            ".diiiijjjjjiiijf..",
            "djiigiijjjiigiiid.",
            "diigiiijjjigiiiid.",
            "diiiikijjjiiikijd.",
            ".diiiijjjjjiiiid..",
            "...jiijiijiijij..."
        ],
        "key": {
            "a": [18, 24, 101],
            "b": [70, 80, 208],
            "c": [161, 165, 211],
            "d": [167, 105, 65],
            "e": [74, 47, 29],
            "f": [167, 129, 104],
            "g": [149, 149, 149],
            "h": [219, 219, 219],
            "i": [66, 66, 66],
            "j": [33, 21, 13],
            "k": [41, 41, 41]
        }
    },
    "alien-tracks-1-left": {
        "pixels": [
            "......abbbbcb.....",
            ".....aaaabbbcb....",
            "....aaaaaaabbdd...",
            "....edddaaaeeeed..",
            "..eeefdddeeeeeeed.",
            ".eeeefeeeeeeeeeed.",
            "eeeeefeeeeeeeeee..",
            "eebbffeeeeeeeee...",
            "bbbeffeeeeeeee....",
            "aaaeeffeeeeeed....",
            "faeeeffeeeeee.....",
            ".ffeeeefffe.......",
            "...fffefghe.......",
            ".....ffggghd......",
            "......ffgged......",
            ".......ffgedd.....",
            ".......ddgggh.....",
            ".....eddddddddd...",
            "....eeeeeeeeeedd..",
            "...eiijiijiijiidd.",
            "..ejiiijjjjjiiiid.",
            ".eiiigiijjjiigiijd",
            ".eiiiigijjjiiigiie",
            ".ejikiiijjjikiiiie",
            "..eiiiijjjjjiiiie.",
            "...jijiijiijiij..."
        ],
        "key": {
            "a": [18, 24, 101],
            "b": [70, 80, 208],
            "c": [161, 165, 211],
            "d": [167, 129, 104],
            "e": [167, 105, 65],
            "f": [74, 47, 29],
            "g": [149, 149, 149],
            "h": [219, 219, 219],
            "i": [66, 66, 66],
            "j": [33, 21, 13],
            "k": [41, 41, 41]
        }
    },
    "alien-tracks-2-right": {
        "pixels": [
            ".....aaabbcb......",
            "....aaaaabbcb.....",
            "...ddaaaaaaabb....",
            "..dddddaaadddd....",
            ".eddddddddddfddd..",
            ".eedddddddddfdddd.",
            "..eeddddddddfddddd",
            "...eddddddddffbbdd",
            "....edddddddffdbbb",
            "....eedddddffddaaa",
            ".....eeedddffdddad",
            ".......eedfdddddd.",
            ".......eghddeee...",
            "......dggghee.....",
            "......ddggee......",
            ".....dddgee.......",
            ".....ggggff.......",
            "...ddddffffff.....",
            "..dddddddddfff....",
            ".ddijjijjijjijf...",
            ".djjjjiiiiijjjjf..",
            "djjjgjjiiijjgjjid.",
            "djjgjjjiiijgjjjjd.",
            "dijjjkjiiijjjkjjd.",
            ".dijjjiiiiijjjid..",
            "...jjijjijjijji..."
        ],
        "key": {
            "a": [18, 24, 101],
            "b": [70, 80, 208],
            "c": [161, 165, 211],
            "d": [167, 105, 65],
            "e": [74, 47, 29],
            "f": [167, 129, 104],
            "g": [149, 149, 149],
            "h": [219, 219, 219],
            "i": [33, 21, 13],
            "j": [66, 66, 66],
            "k": [41, 41, 41]
        }
    },
    "alien-tracks-2-left": {
        "pixels": [
            "......abbbbcb.....",
            ".....aaaabbbcb....",
            "....aaaaaaabbdd...",
            "....edddaaaeeeed..",
            "..eeefdddeeeeeeed.",
            ".eeeefeeeeeeeeeed.",
            "eeeeefeeeeeeeeee..",
            "eebbffeeeeeeeee...",
            "bbbeffeeeeeeee....",
            "aaaeeffeeeeeed....",
            "faeeeffeeeeee.....",
            ".ffeeeefffe.......",
            "...fffefghe.......",
            ".....ffggghd......",
            "......ffgged......",
            ".......ffgedd.....",
            ".......ddgggh.....",
            ".....eddddddddd...",
            "....eeeeeeeeeedd..",
            "...eijiijiijiijdd.",
            "..eiiiijjjjjiiiid.",
            ".ejiigiijjjiigiiid",
            ".eiiiigijjjiiigiie",
            ".eiikiiijjjikiiije",
            "..ejiiijjjjjiiije.",
            "...jiijiijiijii..."
        ],
        "key": {
            "a": [18, 24, 101],
            "b": [70, 80, 208],
            "c": [161, 165, 211],
            "d": [167, 129, 104],
            "e": [167, 105, 65],
            "f": [74, 47, 29],
            "g": [149, 149, 149],
            "h": [219, 219, 219],
            "i": [66, 66, 66],
            "j": [33, 21, 13],
            "k": [41, 41, 41]
        }
    },
    "alien-tracks-3-right": {
        "pixels": [
            ".....aaabbcb......",
            "....aaaaabbcb.....",
            "...ddaaaaaaabb....",
            "..dddddaaadddd....",
            ".eddddddddddfddd..",
            ".eedddddddddfdddd.",
            "..eeddddddddfddddd",
            "...eddddddddffbbdd",
            "....edddddddffdbbb",
            "....eedddddffddaaa",
            ".....eeedddffdddad",
            ".......eedfdddddd.",
            ".......eghddeee...",
            "......dggghee.....",
            "......ddggee......",
            ".....dddgee.......",
            ".....ggggff.......",
            "...ddddffffff.....",
            "..dddddddddfff....",
            ".ddiijjijjijjif...",
            ".djjjjiiiiijjjjf..",
            "djjjgjjiiijjgjjjd.",
            "dijgjjjiiijgjjjid.",
            "dijjjkjiiijjjkjjd.",
            ".djjjjiiiiijjjjd..",
            "...jijjijjijjii..."
        ],
        "key": {
            "a": [18, 24, 101],
            "b": [70, 80, 208],
            "c": [161, 165, 211],
            "d": [167, 105, 65],
            "e": [74, 47, 29],
            "f": [167, 129, 104],
            "g": [149, 149, 149],
            "h": [219, 219, 219],
            "i": [33, 21, 13],
            "j": [66, 66, 66],
            "k": [41, 41, 41]
        }
    },
    "alien-tracks-3-left": {
        "pixels": [
            "......abbbbcb.....",
            ".....aaaabbbcb....",
            "....aaaaaaabbdd...",
            "....edddaaaeeeed..",
            "..eeefdddeeeeeeed.",
            ".eeeefeeeeeeeeeed.",
            "eeeeefeeeeeeeeee..",
            "eebbffeeeeeeeee...",
            "bbbeffeeeeeeee....",
            "aaaeeffeeeeeed....",
            "faeeeffeeeeee.....",
            ".ffeeeefffe.......",
            "...fffefghe.......",
            ".....ffggghd......",
            "......ffgged......",
            ".......ffgedd.....",
            ".......ddgggh.....",
            ".....eddddddddd...",
            "....eeeeeeeeeedd..",
            "...eijjijjijjiidd.",
            "..ejjjjiiiiijjjjd.",
            ".ejjjgjjiiijjgjjjd",
            ".eijjjgjiiijjjgjie",
            ".ejjkjjjiiijkjjjie",
            "..ejjjjiiiiijjjje.",
            "...iijjijjijjij..."
        ],
        "key": {
            "a": [18, 24, 101],
            "b": [70, 80, 208],
            "c": [161, 165, 211],
            "d": [167, 129, 104],
            "e": [167, 105, 65],
            "f": [74, 47, 29],
            "g": [149, 149, 149],
            "h": [219, 219, 219],
            "i": [33, 21, 13],
            "j": [66, 66, 66],
            "k": [41, 41, 41]
        }
    },
    "token-blue": {
        "pixels": [
            "..abcca..",
            ".abbbbca.",
            "abbdddbca",
            "bbdefgdbc",
            "bcdeefdbc",
            "bcdfeedbb",
            "abcdddbba",
            ".abccbba.",
            "..abbba.."
        ],
        "key": {
            "a": [99, 99, 99],
            "b": [149, 149, 149],
            "c": [181, 181, 181],
            "d": [41, 74, 104],
            "e": [65, 119, 167],
            "f": [78, 143, 200],
            "g": [236, 236, 236]
        }
    },
    "token-carrier-blue": {
        "pixels": [
            ".....abcca.....",
            "....abbbbca....",
            "...abbaaabca...",
            "...bbabccabc...",
            "...bcabdcabc...",
            "...bcaebbabb...",
            "...abcaaabba...",
            "....abccbba....",
            "...ffabbbagh...",
            ".ggffffffggghh.",
            "ggggffffggggggh",
            "..g.g.g.g.g.g.."
        ],
        "key": {
            "a": [99, 99, 99],
            "b": [149, 149, 149],
            "c": [181, 181, 181],
            "d": [65, 119, 167],
            "e": [126, 126, 126],
            "f": [1, 80, 1],
            "g": [2, 122, 1],
            "h": [3, 173, 1]
        }
    },
    "alien-rocket-pack": {
        "pixels": [
            "..abbbb......",
            ".aaaaaabbb...",
            ".ccaaaaaaabb.",
            ".dddaaaaabaab",
            "...dddddaaba.",
            ".......aaaba.",
            "......aaaaaa.",
            ".efefaaaadag.",
            "ggfgfaaadaag.",
            ".hhhddadhhgg.",
            ".....ddaahgg.",
            ".....aaaaggg.",
            ".....hhhhgg..",
            "....aaahggg..",
            "....ddddhhh..",
            "....daaacic..",
            "...daaaaccc..",
            "..daaaa..c...",
            "..daaaa......",
            "...daaa......",
            "...daaaa.....",
            "....daaaj....",
            "....daajj....",
            "....klllj....",
            "...klll......",
            "...kll.......",
            "...kk........"
        ],
        "key": {
            "a": [1, 179, 91],
            "b": [1, 226, 115],
            "c": [196, 43, 38],
            "d": [1, 126, 64],
            "e": [1, 90, 206],
            "f": [0, 0, 0],
            "g": [1, 74, 169],
            "h": [1, 56, 129],
            "i": [255, 142, 0],
            "j": [158, 158, 158],
            "k": [95, 95, 95],
            "l": [115, 115, 115]
        }
    },
    "alien-death-01": {
        "pixels": [
            "....aaaaaaa....",
            "...aaaabbbaac..",
            "..aabbbbbbaaac.",
            ".cabbddddbbaaac",
            ".aabbdeeedbbbaa",
            ".abbdeeeeedbbaa",
            "aabbdeeeeedbbaa",
            "aabbddeeedbbbaa",
            "aaabbdddddbbaac",
            "caabbbdbbbbaaa.",
            ".aabbbbbbbaa...",
            ".caabbbbaaac...",
            "..aabbaaa......",
            "...aaaaa.......",
            ".....ccc......."
        ],
        "key": {
            "a": [255, 79, 79],
            "b": [255, 194, 79],
            "c": [126, 125, 125],
            "d": [246, 255, 27],
            "e": [255, 255, 255]
        }
    },
    "alien-death-02": {
        "pixels": [
            "...aaaaaaab....",
            "..baacccaaab...",
            ".bacccccccaab..",
            ".baccdddcccaa..",
            "..bcddeddcccaa.",
            "...bbbeeddccaa.",
            "...babbedddcca.",
            "...bbbeedddcca.",
            "...bcbdddcccca.",
            "....cdddccccca.",
            "....cccccccaaa.",
            "..baaacccaaaab.",
            "...baaaaaaab...",
            ".....baab......",
            "..............."
        ],
        "key": {
            "a": [126, 125, 125],
            "b": [42, 42, 42],
            "c": [255, 79, 79],
            "d": [255, 194, 79],
            "e": [246, 255, 27]
        }
    },
    "alien-death-03": {
        "pixels": [
            "...............",
            "......aaaa.....",
            "....aaaaaaa....",
            "...bbbbbbaaaa..",
            "...bcccbbbaaa..",
            "...bddccbbaaa..",
            ".....dccbbbbaaa",
            "......dcbbbbbaa",
            "......bccbbaaaa",
            "....ddccbbbaaaa",
            "....cccbbbaaaa.",
            ".....bbbbabaa..",
            "......abbbaaa..",
            ".........aaa...",
            "..............."
        ],
        "key": {
            "a": [42, 42, 42],
            "b": [126, 125, 125],
            "c": [255, 79, 79],
            "d": [255, 194, 79]
        }
    },
    "alien-death-04": {
        "pixels": [
            "...............",
            "...............",
            ".....aaa.......",
            "....aaaaaaaa...",
            "....aabbbaaaa..",
            ".....abcbbbaa..",
            "....aaabcbbaaa.",
            "....aaabcbbaa..",
            "....abbccbbba..",
            "....bbbbbbbaaa.",
            "....aabbbbaaa..",
            "....aaaaaaaaa..",
            "....aaaaaaaa...",
            "...............",
            "..............."
        ],
        "key": {
            "a": [42, 42, 42],
            "b": [126, 125, 125],
            "c": [255, 79, 79]
        }
    },
    "alien-death-05": {
        "pixels": [
            "...............",
            "...............",
            "...............",
            "...............",
            "......aaa......",
            ".....aaaaaa....",
            ".....aabaaa....",
            ".....aaabaaa...",
            ".....aaabaaaa..",
            ".....aabbaaaa..",
            ".....aaaaaaa...",
            "...............",
            "...............",
            "...............",
            "..............."
        ],
        "key": {
            "a": [42, 42, 42],
            "b": [126, 125, 125]
        }
    },
    "bullet": {
        "pixels": [
            "aba",
            "bcb",
            "aba"
        ],
        "key": {
            "a": [69, 61, 28],
            "b": [207, 186, 84],
            "c": [207, 207, 207]
        }
    }
};

const PHASER_KEY = {
    "a": [168, 168, 168],
    "b": [255, 255, 255]
};

const PHASER_IMAGES = [
    {
        "pixels": [
            ".aab.",
            "aabbb",
            ".aab."
        ],
        "key": PHASER_KEY
    },
    {
        "pixels": [
            "..aaaabb.",
            "aaaabbbbb",
            "..aaaabb."
        ],
        "key": PHASER_KEY
    },
    {
        "pixels": [
            "...aaaaab.",
            "aaaaaaabbb",
            ".aaaaaabbb",
            "....aaaab."
        ],
        "key": PHASER_KEY
    },
    {
        "pixels": [
            "....aaabb.",
            "..aaaabbbb",
            "aaaaabbbbb",
            "...aaaabb."
        ],
        "key": PHASER_KEY
    },
    {
        "pixels": [
            ".......aaaabbb.",
            "....aaaaabbbccb",
            "aaaaaaabbbbcccc",
            "....aaaaabbbccb",
            ".......aaaabbb."
        ],
        "key": {
            "a": [168, 168, 168],
            "b": [255, 255, 255],
            "c": [203, 226, 230]
        }
    }
];

function image_width(image) {
    return image.pixels[0].length;
}

function image_height(image) {
    return image.pixels.length;
}

function angle_name(vx, vy) {
    const d = (360 + 90 + Math.atan2(vy, vx) * 180 / Math.PI) % 360;
    if (d > 303.5) {
        return "315";
    } else if (d > 281) {
        return "292";
    } else if (d > 258.5) {
        return "270";
    } else if (d > 236) {
        return "247";
    } else {
        return "225";
    }
}

function current_move(motion, t) {
    let tn_acc = 0;
    for (const [tn, move] of motion) {
        tn_acc += tn;
        if (t < tn_acc) {
            return move;
        }
    }
    return null;
}

const SHIP_WIDTH = image_width(IMAGES["ship"]);
const SHIP_HEIGHT = image_height(IMAGES["ship"]);

const ALIEN_UPDATERS = {
    red_flat: function(model, alien) {
        const alien_image = IMAGES[alien.image];
        alien.x -= 0.2;
        if (alien.x + image_width(alien_image) < model.screen.x) {
            alien.dead = true;
            return;
        }
        alien.y = alien.start_y + Math.sin(alien.x * 0.5) * 4;
        maybeAddBullet(alien.x + 6, alien.y + 5, 0.006, alien, model);
    },
    yellow_line: function(model, alien) {
        const new_y = alien.start_y +
            Math.sin(alien.x_off + alien.x * 0.05) * 20;
        const vx = -0.6;
        const vy = new_y - alien.y;
        alien.image = alien.image_base + angle_name(vx, vy)
        const alien_image = IMAGES[alien.image];
        alien.x += vx;
        alien.y = new_y;
        if (alien.x + image_width(alien_image) < model.screen.x) {
            alien.dead = true;
            return;
        }
        maybeAddBullet(alien.x + 7, alien.y + 7, 0.003, alien, model);
    },
    tracks: function(model, alien) {
        const move = current_move(alien.motion, alien.t);
        if (move === 1 || move === -1) {
            alien.x += move;
            alien.frame += 1;
            if (alien.frame > 2) {
                alien.frame = 0;
            }
        }
        alien.t += 1;
        alien.image = (
            alien.image_base
            + (alien.frame + 1)
            + ((move > 0) ? "-right": "-left")
        );
        const alien_image = IMAGES[alien.image];
        if (alien.x + image_width(alien_image) < model.screen.x) {
            alien.dead = true;
            return;
        }
        maybeAddBullet(alien.x + 9, alien.y + 9, 0.006, alien, model);
    },
    tokenCarrier: function(model, alien) {
        alien.t += 1;
        alien.x += 0.25;
        alien.y += 0.25 * current_move(alien.motion, alien.t);
        const alien_image = IMAGES[alien.image];
        if (alien.x + image_width(alien_image) < model.screen.x) {
            alien.dead = true;
            return;
        }
    },
    rocketPack: function(model, alien) {
        alien.t += 0.25;
        const [dx, dy] = current_move(alien.motion, alien.t);
        alien.x += dx;
        alien.y += dy;
        const alien_image = IMAGES[alien.image];
        if (alien.x + image_width(alien_image) < model.screen.x) {
            alien.dead = true;
            return;
        }
    }
};

function maybeAddBullet(x, y, delta, alien, model) {
    alien.bullet_countdown -= delta;
    if (alien.bullet_countdown < 0) {
        alien.bullet_countdown = 0.5 + Math.random() * 0.5;
        const dir_x = model.ship.x + 5 - x;
        const dir_y = model.ship.y + 1 - y;
        const dist = Math.sqrt(dir_x*dir_x + dir_y*dir_y);
        const speed_mult = 0.6 / dist;
        const vx = dir_x * speed_mult;
        const vy = dir_y * speed_mult;
        model.bullets.push([x, y, vx + SCROLL_SPEED, vy]);
    }
}

const ALIEN_COLLIDERS = {
    weak: function(model, alien) {
        if (alien.dead === true) {
            return;
        }
        if (alien.dead !== false) {
            alien.dead += 1;
            if (alien.dead > 5) {
                alien.dead = true;
            }
            return;
        }

        const alien_image = IMAGES[alien.image];

        for (const bullet of model.ship.bullets) {
            if (
                images_overlap(
                    PHASER_IMAGES[bullet.size], bullet.x, bullet.y,
                    alien_image, alien.x, alien.y
                )
            ) {
                if (bullet.size < 1) {
                    bullet.dead = true;
                }
                alien.dead = 1;
                return;
            }
        }

        if (model.ship.dead === false) {
            if (model.ship.upgrades !== "") {
                const nose_attach_image = IMAGES["ship-nose-attach"];
                if (
                    images_overlap(
                        nose_attach_image, model.ship.x + 12, model.ship.y,
                        alien_image, alien.x, alien.y
                    )
                ) {
                    alien.dead = 1;
                }
            }

            const ship_image = IMAGES["ship"];
            if (
                images_overlap(
                    ship_image, model.ship.x, model.ship.y,
                    alien_image, alien.x, alien.y
                )
            ) {
                alien.dead = 1;
                model.ship.dead = 1;
            }
        }
    },
    tokenCarrier: function(model, alien) {
        const d = alien.dead;
        ALIEN_COLLIDERS.weak(model, alien);
        if (d === false && alien.dead !== false) {
            model.tokens.push([alien.x + 3, alien.y]);
        }
    }
};

function newRedFlat(x, y) {
    return {
        spawn_x: x - 10,
        start_y: y,
        x: x,
        y: y,
        update: ALIEN_UPDATERS.red_flat.name,
        collide: ALIEN_COLLIDERS.weak.name,
        image: "alien-red-flat"
    };
}

function newYellowLine(x, y, x_off) {
    return {
        spawn_x: x - 10,
        start_y: y,
        x: x,
        y: y,
        x_off: x_off,
        update: ALIEN_UPDATERS.yellow_line.name,
        collide: ALIEN_COLLIDERS.weak.name,
        image_base: "alien-yellow-line-"
    };
}

function newTracks(x, y, motion) {
    return {
        spawn_x: x - 10,
        start_x: x,
        x: x,
        y: y,
        motion: motion,
        t: 0,
        frame: 0,
        update: ALIEN_UPDATERS.tracks.name,
        collide: ALIEN_COLLIDERS.weak.name,
        image_base: "alien-tracks-"
    };
}

function newTokenCarrier(x, y, motion) {
    return {
        spawn_x: x - 10,
        x: x,
        y: y,
        motion: motion,
        t: 0,
        update: ALIEN_UPDATERS.tokenCarrier.name,
        collide: ALIEN_COLLIDERS.tokenCarrier.name,
        image: "token-carrier-blue"
    };
}

function newRocketPack(x, y, motion) {
    return {
        spawn_x: x - 10,
        x: x,
        y: y,
        motion: motion,
        t: 0,
        update: ALIEN_UPDATERS.rocketPack.name,
        collide: ALIEN_COLLIDERS.weak.name,
        image: "alien-rocket-pack"
    };
}

const LEVELS = [
    {
        scenery: [
            { x:   0, y: 80, image: "machinery-20x16-01"},
            { x:  20, y: 88, image: "machinery-20x08-01"},
            { x:  40, y: 88, image: "machinery-20x08-02"},
            { x:  60, y: 88, image: "machinery-20x08-03"},
            { x:  80, y: 72, image: "machinery-20x24-01"},
            { x: 100, y: 80, image: "machinery-20x16-01"},
            { x: 120, y: 88, image: "machinery-20x08-03"},
            { x: 140, y: 88, image: "machinery-20x08-02"},
            { x: 160, y: 88, image: "machinery-20x08-01"},
            { x: 180, y: 88, image: "machinery-20x08-01"},
            { x: 200, y: 88, image: "machinery-20x08-03"},
            { x: 220, y: 80, image: "machinery-20x16-01"},
            { x: 240, y: 88, image: "machinery-20x08-03"},
            { x: 260, y: 72, image: "machinery-20x24-01"},
            { x: 280, y: 88, image: "machinery-20x08-02"},
            { x: 300, y: 88, image: "machinery-20x08-01"},
            { x: 320, y: 88, image: "machinery-20x08-03"},
            { x: 340, y: 88, image: "machinery-20x08-03"},
            { x: 360, y: 88, image: "machinery-20x08-02"},
            { x: 380, y: 88, image: "machinery-20x08-03"},
            { x: 400, y: 88, image: "machinery-20x08-01"},
            { x: 420, y: 80, image: "machinery-20x16-01"},
            { x: 440, y: 88, image: "machinery-20x08-03"}
        ],
        aliens: [
            /*newRedFlat(130, 20),
            newRedFlat(145, 25),
            newRedFlat(160, 15),
            newRedFlat(175, 25),
            newRedFlat(210, 50),
            newRedFlat(225, 55),
            newRedFlat(240, 45),
            newRedFlat(255, 55),
            newYellowLine(280, 25, -100),
            newYellowLine(287, 25, -100),
            newYellowLine(294, 25, -100),
            newYellowLine(301, 25, -100),
            newYellowLine(308, 25, -100),
            newYellowLine(315, 25, -100),
            newTracks(
                320,
                62,
                [
                    [38, MOVE_L],
                    [25, STOP_R],
                    [79, MOVE_R],
                    [25, STOP_L],
                    [40, MOVE_R]
                ]
            ),
            newTokenCarrier(330, 20, [[100, MOVE_D], [100, MOVE_U]]),*/
            newRocketPack(
                150,
                20,
                [
                    [40, [10, 40]],
                    [40, [10, -20]]
                ]
            )
        ]
    }
];

// Maybe should be in Smolpxl:

function images_overlap(image1, x1, y1, image2, x2, y2) {
    const rx1 = Math.round(x1);
    const ry1 = Math.round(y1);
    const rx2 = Math.round(x2);
    const ry2 = Math.round(y2);
    const width1 = image_width(image1);
    const width2 = image_width(image2);
    const height1 = image_height(image1);
    const height2 = image_height(image2);

    if (
        rx1 + width1 < rx2 ||
        rx2 + width2 < rx1 ||
        ry1 + height1 < ry2 ||
        ry2 + height2 < ry1
    ) {
        return false;
    }

    const startx = Math.max(rx1, rx2);
    const starty = Math.max(ry1, ry2);
    const endx = Math.min(rx1 + width1, rx2 + width2);
    const endy = Math.min(ry1 + height1, ry2 + height2);

    const pixels1 = image1.pixels;
    const pixels2 = image2.pixels;

    for (let x = startx; x < endx; x++) {
        for (let y = starty; y < endy; y++) {
            const ch1 = pixels1[y - ry1][x - rx1];
            const ch2 = pixels2[y - ry2][x - rx2];
            if (ch1 !== '.' && ch2 !== '.') {
                return true;
            }
        }
    }
    return false;
}

// Model

function new10Stars(list, speed) {
    for (let i = 0; i < 10; i++) {
        list.push([
            speed,
            Smolpxl.randomInt(0, WIDTH - 1),
            Smolpxl.randomInt(0, HEIGHT - 1)
        ]);
    }
}

function newStars() {
    let ret = [];
    new10Stars(ret, 0.05);
    new10Stars(ret, 0.125);
    new10Stars(ret, 0.25);
    return ret;
}

function newModel(oldModel, next_level) {
    return {
        screen: {
            x: 0
        },
        ship: {
            x: 20,
            y: (HEIGHT / 2) - (IMAGES["ship"].pixels.length / 2),
            dead: false,  // true, false, or a number while dying
            phaser: 0,
            bullets: [],
            lives: 3,
            upgrades: ""
        },
        aliens: [],
        bullets: [],
        tokens: [],
        level: 0,
        keys: {
            UP: false,
            DOWN: false,
            LEFT: false,
            RIGHT: false,
            FIRE: false
        },
        stars: newStars()
    };
}

// Update

function updateInput(runningGame, model) {
    for (const input of runningGame.input()) {
        if (input.name === "UP") {
            model.keys.UP = true;
        } else if (input.name === "RELEASE_UP") {
            model.keys.UP = false;
        } else if (input.name === "DOWN") {
            model.keys.DOWN = true;
        } else if (input.name === "RELEASE_DOWN") {
            model.keys.DOWN = false;
        } else if (input.name === "LEFT") {
            model.keys.LEFT = true;
        } else if (input.name === "RELEASE_LEFT") {
            model.keys.LEFT = false;
        } else if (input.name === "RIGHT") {
            model.keys.RIGHT = true;
        } else if (input.name === "RELEASE_RIGHT") {
            model.keys.RIGHT = false;
        } else if (input.name === "BUTTON1") {
            model.keys.FIRE = true;
        } else if (input.name === "RELEASE_BUTTON1") {
            model.keys.FIRE = false;
        }
    }
}

function updatePhaser(model) {
    if (model.keys.FIRE) {
        model.ship.phaser += 2;
        if (model.ship.phaser > 100) {
            model.ship.phaser = 100;
        }
    } else {
        if (model.ship.phaser > 0) {
            const size = Math.floor(model.ship.phaser / 25);
            const offset = size > 3 ? 0 : 1;
            model.ship.bullets.push({
                x: model.ship.x + 11,
                y: model.ship.y + offset,
                size: size,
                dead: false
            });
        }
        model.ship.phaser = 0;
    }
}

function updateShipBullets(model) {
    const level = LEVELS[model.level];
    for (const bullet of model.ship.bullets) {
        bullet.x += 10;
        if (bullet.x > model.screen.x + WIDTH) {
            bullet.dead = true;
        } else {
            const bullet_image = PHASER_IMAGES[bullet.size];
            for (const scenery of level.scenery) {
                const image = IMAGES[scenery.image]
                if (
                    images_overlap(
                        image,
                        scenery.x,
                        scenery.y,
                        bullet_image,
                        bullet.x,
                        bullet.y
                    )
                ) {
                    bullet.dead = true;
                }
            }
        }
    }
    model.ship.bullets = model.ship.bullets.filter(
        bullet => bullet.dead !== true);
}

function updateMoveShip(model) {
    const rel_x = model.ship.x - model.screen.x;

    if (model.keys.UP && model.ship.y > 0) {
        model.ship.y -= 1;
    }
    if (model.keys.DOWN && model.ship.y + SHIP_HEIGHT < HEIGHT) {
        model.ship.y += 1;
    }
    if (model.keys.LEFT && rel_x > 0) {
        model.ship.x -= 1;
    }
    if ( model.keys.RIGHT && rel_x + SHIP_WIDTH < WIDTH) {
        model.ship.x += 1;
    }
}

function updateTokens(model) {
    const token_image = IMAGES["token-blue"];
    const ship_image = IMAGES["ship"];
    const ship_x = model.ship.x;
    const ship_y = model.ship.y;
    if (model.ship.dead === false) {
        for (const token of model.tokens) {
            if (
                images_overlap(
                    ship_image,
                    ship_x,
                    ship_y,
                    token_image,
                    token[0],
                    token[1]
                )
            ) {
                token.dead = true;
                model.ship.upgrades = "nose-attach";
            }
        }
    }
    model.tokens = model.tokens.filter(
        token => token.dead !== true);
}

function updateScroll(model) {
    model.screen.x += SCROLL_SPEED;
    model.ship.x += SCROLL_SPEED;

    model.stars = model.stars.map(
        star => (
            star[1] - (model.screen.x * star[0]) < 0
                ? [star[0], star[1] + WIDTH, star[2]]
                : star
        )
    );
}

function updateDyingShip(model) {
    if (model.ship.dead !== false && model.ship.dead !== true) {
        model.ship.dead += 0.5;
        if (model.ship.dead > 5) {
            model.ship.dead = true;
        }
    }
}

function updateCollideWithScenery(model) {
    const level = LEVELS[model.level];
    const ship_image = IMAGES["ship"];
    for (const scenery of level.scenery) {
        const image = IMAGES[scenery.image]
        if (
            images_overlap(
                image,
                scenery.x,
                scenery.y,
                ship_image,
                model.ship.x,
                model.ship.y
            )
        ) {
            model.ship.dead = 1;
        }
    }
}

function newAlien(alien_spec) {
    const alien = JSON.parse(JSON.stringify(alien_spec));
    delete alien.spawn_x;
    alien.dead = false; // true, false or a number while dying
    alien.bullet_countdown = Math.random();
    return alien;
}

function updateNewAliens(model) {
    const level = LEVELS[model.level];
    for (const alien_spec of level.aliens) {
        const d = model.screen.x + WIDTH - alien_spec.spawn_x;
        if (0 < d && d <= 0.5) {
            model.aliens.push(newAlien(alien_spec));
        }
    }
}

function updateAliens(model) {
    for (const alien of model.aliens) {
        const fn = ALIEN_UPDATERS[alien.update];
        fn(model, alien);
    }
}

function updateBullets(model) {
    for (const bullet of model.bullets) {
        bullet[0] += bullet[2];
        bullet[1] += bullet[3];
    }
}

function updateAlienCollisions(model) {
    for (const alien of model.aliens) {
        const fn = ALIEN_COLLIDERS[alien.collide];
        fn(model, alien);
    }
    model.aliens = model.aliens.filter(alien => alien.dead !== true);
}

function updateBulletCollisions(model) {
    const bullet_image = IMAGES["bullet"];
    const ship_image = IMAGES["ship"];
    const nose_attach_image = IMAGES["ship-nose-attach"];
    const ship_x = model.ship.x;
    const ship_y = model.ship.y;
    if (model.ship.dead === false) {
        for (const bullet of model.bullets) {
            if (
                model.ship.upgrades !== "" &&
                images_overlap(
                    nose_attach_image,
                    ship_x + 12,
                    ship_y,
                    bullet_image,
                    bullet[0],
                    bullet[1]
                )
            ) {
                bullet.dead = true;
            } else if (
                images_overlap(
                    ship_image,
                    ship_x,
                    ship_y,
                    bullet_image,
                    bullet[0],
                    bullet[1]
                )
            ) {
                bullet.dead = true;
                model.ship.dead = 1;
            }
        }
    }

    const level = LEVELS[model.level];
    for (const bullet of model.bullets) {
        for (const scenery of level.scenery) {
            const image = IMAGES[scenery.image]
            if (
                images_overlap(
                    image,
                    scenery.x,
                    scenery.y,
                    bullet_image,
                    bullet[0],
                    bullet[1]
                )
            ) {
                bullet.dead = true;
            }
        }
    }
    model.bullets = model.bullets.filter(bullet => bullet.dead !== true);
}

function update(runningGame, model) {
    updateDyingShip(model);
    updateScroll(model);
    updateNewAliens(model);
    updateAliens(model);
    updateBullets(model);
    updateShipBullets(model);
    if (model.ship.dead === false) {
        updateInput(runningGame, model);
        updatePhaser(model);
        updateMoveShip(model);
        updateTokens(model);
        updateCollideWithScenery(model);
    }
    updateAlienCollisions(model);
    updateBulletCollisions(model);
    return model;
}

// View

function starColor(speed) {
    return [300 * speed, 300 * speed, 500 * speed];
}

function viewStars(screen, model) {
    for (const [speed, x, y] of model.stars) {
        screen.set(
            Math.floor(x) - Math.floor(model.screen.x * speed),
            y,
            starColor(speed)
        );
    }
}

function viewShip(screen, model) {
    if (model.ship.dead === false) {
        const ship_image = IMAGES["ship"];
        screen.draw(
            Math.floor(model.ship.x) - Math.floor(model.screen.x),
            model.ship.y,
            ship_image.pixels,
            ship_image.key
        );
        if (model.ship.upgrades !== "") {
            const attach_image = IMAGES["ship-nose-attach"];
            screen.draw(
                Math.floor(model.ship.x + 12) - Math.floor(model.screen.x),
                model.ship.y,
                attach_image.pixels,
                attach_image.key
            );
        }
    } else if (model.ship.dead !== true) {
        const ship_image = IMAGES[`ship-death-0${Math.round(model.ship.dead)}`];
        screen.draw(
            Math.floor(model.ship.x) - Math.floor(model.screen.x) - 2,
            model.ship.y - 2,
            ship_image.pixels,
            ship_image.key
        );
    }
}

function viewShipBullets(screen, model) {
    for (const bullet of model.ship.bullets) {
        const bullet_image = PHASER_IMAGES[bullet.size];
        screen.draw(
            Math.floor(bullet.x) - Math.floor(model.screen.x),
            bullet.y,
            bullet_image.pixels,
            bullet_image.key
        );
    }
}

function viewTokens(screen, model) {
    for (const token of model.tokens) {
        const token_image = IMAGES["token-blue"];
        screen.draw(
            Math.floor(token[0]) - Math.floor(model.screen.x),
            token[1],
            token_image.pixels,
            token_image.key
        );
    }
}

function viewAliens(screen, model) {
    for (const alien of model.aliens) {
        if (alien.dead === false) {
            const alien_image = IMAGES[alien.image];
            screen.draw(
                Math.floor(alien.x) - Math.floor(model.screen.x),
                alien.y,
                alien_image.pixels,
                alien_image.key
            );
        } else if (alien.dead !== true) {
            const image = IMAGES[`alien-death-0${Math.round(alien.dead)}`];
            screen.draw(
                Math.floor(alien.x) - Math.floor(model.screen.x) - 1,
                alien.y - 1,
                image.pixels,
                image.key
            );
        }
    }
}

function viewBullets(screen, model) {
    const image = IMAGES["bullet"];
    for (const bullet of model.bullets) {
        screen.draw(
            Math.floor(bullet[0] - model.screen.x),
            Math.floor(bullet[1]),
            image.pixels,
            image.key
        );
    }
}

function viewScenery(screen, model) {
    const level = LEVELS[model.level];
    for (const scenery of level.scenery) {
        const relx = Math.floor(scenery.x) - Math.floor(model.screen.x);
        if (relx > WIDTH) {
            break;  // Assuming scenery is sorted, we are done
        }
        const image = IMAGES[scenery.image]
        if (relx + image_width(image) >= 0) {
            screen.draw(
                relx,
                scenery.y,
                image.pixels,
                image.key
            );
        }
    }
}

function viewBottomBar(screen, model) {
    const life_image = IMAGES["life"];
    const life_width = image_width(life_image);
    screen.rect(0, HEIGHT, WIDTH, BOTTOM_BAR_HEIGHT, Smolpxl.colors.BLACK);
    const gap = Math.floor(Math.min(50 / model.ship.lives, 10));
    for (let i = 0; i < model.ship.lives; ++i) {
        const x = 1 + i * gap;
        screen.draw(x, HEIGHT, life_image.pixels, life_image.key);
        if (x + life_width > 49) {
            screen.rect(
                49, HEIGHT, 10, BOTTOM_BAR_HEIGHT, Smolpxl.colors.BLACK);
        }
    }
    const phaser_len = 45;
    screen.rect(51, HEIGHT, phaser_len + 2, 1, PHASER_OUTLINE_COL);
    screen.rect(
        51,
        HEIGHT + BOTTOM_BAR_HEIGHT - 1,
        phaser_len + 2,
        1,
        PHASER_OUTLINE_COL
    );
    screen.rect(50, HEIGHT + 1, 2, 2, PHASER_OUTLINE_COL);
    screen.rect(51 + phaser_len + 1, HEIGHT + 1, 2, 2, PHASER_OUTLINE_COL);

    if (model.ship.phaser > 0) {
        const len = Math.round(45 * model.ship.phaser / 100);
        screen.rect(52, HEIGHT + 1, len, 1, PHASER_TOP_COL);
        screen.rect(52, HEIGHT + 2, len - 1, 1, PHASER_BOTTOM_COL);
        screen.set(
            52 + Math.min(len, phaser_len - 1), HEIGHT + 1, PHASER_END_COL);
        if (len > 0) {
            screen.set(52 + len - 1, HEIGHT + 2, PHASER_END_COL);
        }
    }
}

function view(screen, model) {
    viewStars(screen, model);
    viewScenery(screen, model);
    viewBullets(screen, model);
    viewTokens(screen, model);
    viewAliens(screen, model);
    viewShipBullets(screen, model);
    viewShip(screen, model);
    viewBottomBar(screen, model);
}

// Game

const game = new Smolpxl.Game();
game.setBorderColor([15, 15, 15]);
//game.sendPopularityStats();
game.setFps(30);
game.showSmolpxlBar();
game.setSourceCodeUrl(
    "https://gitlab.com/smolpxl/smolpxl/-/blob/master/public/rightwaves/game.js"
);
game.setSize(WIDTH, HEIGHT + BOTTOM_BAR_HEIGHT);
game.setTitle("Rightwaves");

game.start("rightwaves", newModel(), view, update);
