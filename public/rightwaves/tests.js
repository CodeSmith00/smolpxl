describe("Rightwaves", function() {

    describe("images_overlap", function() {

        const W3x3 = {
            "pixels": [
              "aaa",
              "aaa",
              "aaa"
            ],
            "key": {
              "a": Smolpxl.colors.WHITE
            }
        };

        const DOT = {
            "pixels": [
              "...",
              ".a.",
              "..."
            ],
            "key": {
              "a": Smolpxl.colors.WHITE
            }
        };

        const TRANS = {
            "pixels": [
              "...",
              "...",
              "..."
            ],
            "key": {}
        };

        it(
            "says images don't collide if the rectangles don't overlap",
            function() {

            expect(images_overlap(W3x3,  0,  0, W3x3, 10, 10)).toBeFalse();
            expect(images_overlap(W3x3, 10, 10, W3x3,  0,  0)).toBeFalse();
            expect(images_overlap(W3x3,  0,  0, W3x3,  3,  0)).toBeFalse();
            expect(images_overlap(W3x3, 10,  0, W3x3,  7,  0)).toBeFalse();
            expect(images_overlap(W3x3, 10, 10, W3x3, 10, 13)).toBeFalse();
            expect(images_overlap(W3x3, 10, 13, W3x3, 10, 10)).toBeFalse();
        });

        it("says solid images collide if the rectangles overlap", function() {
            expect(images_overlap(W3x3,  0,  0, W3x3,  0,  0)).toBeTrue();
            expect(images_overlap(W3x3,  0,  0, W3x3,  2,  0)).toBeTrue();
            expect(images_overlap(W3x3,  0,  0, W3x3,  0,  2)).toBeTrue();
            expect(images_overlap(W3x3,  0,  0, W3x3,  2,  2)).toBeTrue();
            expect(images_overlap(W3x3, 10, 10, W3x3, 10, 10)).toBeTrue();
            expect(images_overlap(W3x3, 10, 10, W3x3,  8, 10)).toBeTrue();
            expect(images_overlap(W3x3, 10, 10, W3x3, 10,  8)).toBeTrue();
            expect(images_overlap(W3x3, 10, 10, W3x3,  8,  8)).toBeTrue();
        });

        it("never considers a transparent image to overlap", function() {
            expect(images_overlap(W3x3,  0, 0, TRANS, 0, 0)).toBeFalse();
            expect(images_overlap(TRANS, 0, 0, W3x3,  0, 0)).toBeFalse();
        });

        it("accurately says semitransparent images overlap solid", function() {
            expect(images_overlap(W3x3, 100, 100, DOT, 102, 101)).toBeFalse();
            expect(images_overlap(W3x3, 100, 100, DOT, 101, 101)).toBeTrue();
            expect(images_overlap(W3x3, 100, 100, DOT, 100, 101)).toBeTrue();
            expect(images_overlap(W3x3, 100, 100, DOT,  99, 101)).toBeTrue();
            expect(images_overlap(W3x3, 100, 100, DOT,  98, 101)).toBeFalse();

            expect(images_overlap(DOT, 100, 100, W3x3, 102,  99)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, W3x3, 101,  99)).toBeTrue();
            expect(images_overlap(DOT, 100, 100, W3x3, 100,  99)).toBeTrue();
            expect(images_overlap(DOT, 100, 100, W3x3,  99,  99)).toBeTrue();
            expect(images_overlap(DOT, 100, 100, W3x3,  98,  99)).toBeFalse();
        });

        it("accurately says semitransparent images overlap", function() {
            expect(images_overlap(DOT, 100, 100, DOT,  99,  99)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, DOT, 100,  99)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, DOT, 101,  99)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, DOT,  99, 100)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, DOT, 100, 100)).toBeTrue();
            expect(images_overlap(DOT, 100, 100, DOT, 101, 100)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, DOT,  99, 101)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, DOT, 100, 101)).toBeFalse();
            expect(images_overlap(DOT, 100, 100, DOT, 101, 101)).toBeFalse();
        });
    });
});
